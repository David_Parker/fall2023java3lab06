// David Parker
package backend;

import java.util.Random;

public class RpsGame {
    private int wins;
    private int ties;
    private int losses;
    private Random rand;

    public RpsGame() {
        this.wins = 0;
        this.ties = 0;
        this.losses = 0;
        this.rand = new Random();
    }

    public int getWins() {
        return this.wins;
    }

    public int getTies() {
        return this.ties;
    }

    public int getLosses() {
        return this.losses;
    }

    public String playRound(String userAction) {
        String computerAction = "";
        String winner = "";

        int computerChoice = this.rand.nextInt(3);
        switch (computerChoice) {
            case 0:
                computerAction = "rock";
                break;
            case 1:
                computerAction = "scissors";
                break;
            case 2:
                computerAction = "paper";
                break;
            default:
                throw new IllegalArgumentException();
        }
        boolean userWon = checkWinner(computerAction, userAction);
        boolean compWon = checkWinner(userAction, computerAction);
        if (userWon) {
            winner = "the user wins!";
            this.wins++;
        } else if (compWon) {
            winner = "the computer wins.";
            this.losses++;
        } else {
            winner = "it's a tie!";
            this.ties++;
        }
        return "Computer plays " + computerAction + " and " + winner;
    }

    public boolean checkWinner(String compInput, String userInput) {
        if (compInput.equalsIgnoreCase("rock") && userInput.equalsIgnoreCase("paper")) {
            return true;
        } else if (compInput.equalsIgnoreCase("paper") && userInput.equalsIgnoreCase("scissors")) {
            return true;
        } else if (compInput.equalsIgnoreCase("scissors") && userInput.equalsIgnoreCase("rock")) {
            return true;
        } else {
            return false;
        }
    }

    public String toString() {
        return "Wins: " + this.wins + " Ties: " + this.ties + " Losses: " + this.losses;
    }
}
