// David Parker
package rpsgame;

import java.util.Scanner;

import backend.RpsGame;

/**
 * Console application for rock paper scissors game
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        RpsGame game = new RpsGame();
        Scanner reader = new Scanner(System.in);
        boolean isPlaying = true;
        while (isPlaying) {
            System.out.println(game.playRound(userGameChoice()));
            System.out.println(game);
            if (!keepPlaying()) {
                isPlaying = false;
            }
        }
        reader.close();
    }

    public static String userGameChoice() {
        Scanner reader = new Scanner(System.in);
        String input = "";
        while (true) {
            System.out.println("Rock, paper, or scissors?");
            input = reader.nextLine();
            if (input.equalsIgnoreCase("rock") || input.equalsIgnoreCase("paper") || input.equalsIgnoreCase("scissors")) {
                return input;
            } else {
                System.out.println("Invalid input!");
            }
        }
    }

    public static boolean keepPlaying() {
        Scanner reader = new Scanner(System.in);
        String input = "";
        while (true) {
            System.out.println("Do you want to keep playing?(y,n)");
            input = reader.nextLine();
            if (input.equalsIgnoreCase("n")) {
                return false;
            } else if (input.equalsIgnoreCase("y")) {
                return true;
            } else {
                System.out.println("Invalid input!");
            }
        }
    }
}
